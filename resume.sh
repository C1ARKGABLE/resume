#!/bin/bash


if [ -z "$1" ]; then
    company="You"
    else
    company="$1"
fi

var_path=`dirname "$0"`
cd $var_path

xelatex "\\def\\companyName{$company}\\input CV.tex" > last_log.txt
mv "$var_path/CV.pdf" "/Users/g7/resumes/GableBrown_$company.pdf"

echo "$company@GableBrown.com"
echo "$company@GableBrown.com" | pbcopy
echo "https://www.gablebrown.com"
echo "https://www.linkedin.com/in/gable-brown"
echo "https://gitlab.com/C1ARKGABLE"
rm GableBrown_*